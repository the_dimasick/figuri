public class Pryamougolnik extends Figuri {
    public  Pryamougolnik( double dlina, double visota) {
        super(dlina,visota);
    }
    @Override
    public double getPloshad() {
        double ploshad = (getDlina()*getHeight());
        return ploshad;
    }
}

