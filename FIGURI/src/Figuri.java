public abstract class Figuri {
    private double mDlina;
    private double mVisota;
    public Figuri ( double dlina, double visota) {
        mDlina = dlina;
        mVisota = visota;
    }
            public double getDlina(){
            return mDlina;
    }
                public double getHeight() {
                return mVisota;
    }
    public abstract double getPloshad();
}

