public class Treugolnik extends Figuri {
    public Treugolnik( double dlina, double visota) {
        super(dlina,visota);
    }
    @Override
    public double getPloshad() {
        double ploshad = (getDlina()*getHeight())/2;
        return ploshad;
    }
}